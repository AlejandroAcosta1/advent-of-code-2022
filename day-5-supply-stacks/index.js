const readFile = require("../read-file");

readFile().then(supplyStacks);

function supplyStacks(data) {
    const [initialStack, moves] = data
        .split("\n\n")
        .map((item) => item.split("\n"));

    // Pop last line and use it to determine length
    // initialStack will now only contain the data rows
    const stackCount = initialStack.pop().split("   ").length;

    const stacks = initialStack.reduce((stackAcc, line) => {
        for (let i = 0; i < stackCount; i++) {
            if (!stackAcc[i]) {
                stackAcc[i] = [];
            }
            const stackEntry = line.slice(i * 4, i * 4 + 3).trim();
            if (stackEntry) {
                stackAcc[i].push(stackEntry[1]);
            }
        }
        return stackAcc;
    }, []);

    const parsedMoves = moves
        .filter((x) => x)
        .map((move) => move.match(/move (\d+) from (\d+) to (\d+)/));

    const crateMover9000Stacks = crateMover9000(
        JSON.parse(JSON.stringify(stacks)),
        parsedMoves
    );
    const crateMover9001Stacks = crateMover9001(
        JSON.parse(JSON.stringify(stacks)),
        parsedMoves
    );

    const topStack = displayTopStack(crateMover9000Stacks);
    const updatedTopStack = displayTopStack(crateMover9001Stacks);
    console.log(
        `After the rearrangement the following crates end up on top: ${topStack}`
    );
    console.log(
        `After the updated rearrangement the following crates end up on top: ${updatedTopStack}`
    );
}

function crateMover9000(stacks, moves) {
    moves.forEach((move) => {
        const [, amount, source, target] = move;

        for (let i = 0; i < amount; i++) {
            const box = stacks[source - 1].shift();
            stacks[target - 1].unshift(box);
        }
    });
    return stacks;
}

function crateMover9001(stacks, moves) {
    moves.forEach((move) => {
        const [, amount, source, target] = move;

        const boxes = stacks[source - 1].splice(0, amount);
        stacks[target - 1].splice(0, 0, ...boxes);
    });
    return stacks;
}

function displayTopStack(stacks) {
    return stacks.map((stack) => stack[0]).join("");
}