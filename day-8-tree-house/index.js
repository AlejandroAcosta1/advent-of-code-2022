const readFile = require("../read-file");

readFile().then(treehouse);

function treehouse(data) {
  const treeMap = data
    .split("\n")
    .filter((x) => x)
    .map((line) => line.split("").map((height) => parseInt(height)));

  const visibleTreeCount = getVisibleTreeCount(treeMap);
  const scenicScore = getScenicScore(treeMap);

  console.log(
    `The number of trees visible from outside the grid is ${visibleTreeCount}`
  );
  console.log(
    `The highest possible scenic score for any tree is ${scenicScore}`
  );
}

function getScenicScore(treeMap) {
  let highestScore = 0;
  for (let i = 1; i < treeMap.length - 1; i++) {
    const row = treeMap[i];
    for (let j = 1; j < row.length - 1; j++) {
      const height = treeMap[i][j];

      // Scan for scenery score
      let topViewingDistance = 0;
      for (let k = i - 1; k >= 0; k--) {
        const blockingTreeHeight = treeMap[k][j];
        topViewingDistance += 1;
        if (blockingTreeHeight >= height) {
          break;
        }
      }

      let leftViewingDistance = 0;
      for (let k = j - 1; k >= 0; k--) {
        const blockingTreeHeight = treeMap[i][k];
        leftViewingDistance += 1;
        if (blockingTreeHeight >= height) {
          break;
        }
      }

      let bottomViewingDistance = 0;
      for (let k = i + 1; k < treeMap.length; k++) {
        const blockingTreeHeight = treeMap[k][j];
        bottomViewingDistance += 1;
        if (blockingTreeHeight >= height) {
          break;
        }
      }

      let rightViewingDistance = 0;
      for (let k = j + 1; k < row.length; k++) {
        const blockingTreeHeight = treeMap[i][k];
        rightViewingDistance += 1;
        if (blockingTreeHeight >= height) {
          break;
        }
      }
      score =
        topViewingDistance *
        leftViewingDistance *
        bottomViewingDistance *
        rightViewingDistance;

      highestScore = Math.max(score, highestScore);
    }
  }
  return highestScore;
}

function getVisibleTreeCount(treeMap) {
  // visible by default
  const outerRingCount = 2 * treeMap.length + 2 * (treeMap[0].length - 2);

  let visibleTreeCount = outerRingCount;
  for (let i = 1; i < treeMap.length - 1; i++) {
    const row = treeMap[i];
    for (let j = 1; j < row.length - 1; j++) {
      const height = treeMap[i][j];

      let topBlocking = false;
      for (let k = i - 1; k >= 0; k--) {
        const blockingTree = treeMap[k][j];
        if (blockingTree >= height) {
          topBlocking = true;
        }
      }

      let leftBlocking = false;
      for (let k = j - 1; k >= 0; k--) {
        const blockingTree = treeMap[i][k];
        if (blockingTree >= height) {
          leftBlocking = true;
        }
      }

      let bottomBlocking = false;
      for (let k = i + 1; k < treeMap.length; k++) {
        const blockingTree = treeMap[k][j];
        if (blockingTree >= height) {
          bottomBlocking = true;
        }
      }

      let rightBlocking = false;
      for (let k = j + 1; k < row.length; k++) {
        const blockingTree = treeMap[i][k];
        if (blockingTree >= height) {
          rightBlocking = true;
        }
      }

      let isVisible = !(
        topBlocking &&
        leftBlocking &&
        bottomBlocking &&
        rightBlocking
      );

      if (isVisible) {
        visibleTreeCount += 1;
      }
    }
  }
  return visibleTreeCount;
}
