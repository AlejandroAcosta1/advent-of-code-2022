const readFile = require("../read-file");

readFile().then(campCleanup);

function campCleanup(data) {
    const assignmentPairs = data
        .split("\n")
        .filter(x => x)
        .map(pair =>
            pair.split(",")
                .map(elf => elf.split("-")
                    .map(id => parseInt(id))
                )
        );

    const fullyOverlappingAssignmentPairCount = assignmentPairs.reduce((count, pair) => {
        const [
            [firstElfBegin, firstElfEnd],
            [secondElfBegin, secondElfEnd]
        ] = pair;

        const firstElfRedundant = firstElfBegin >= secondElfBegin &&
            firstElfEnd <= secondElfEnd
        const secondElfRedundant = secondElfBegin >= firstElfBegin &&
            firstElfEnd >= secondElfEnd;

        return firstElfRedundant || secondElfRedundant ? count + 1 : count;
    }, 0)

    const overlappingAssignmentPairCount = assignmentPairs.reduce((count, pair) => {
        const [
            [firstElfBegin, firstElfEnd],
            [secondElfBegin, secondElfEnd]
        ] = pair;

        const firstElfRedundant = (firstElfBegin <= secondElfBegin && secondElfBegin <= firstElfEnd) ||
            (firstElfBegin <= secondElfEnd && secondElfEnd <= firstElfEnd);
        const secondElfRedundant = (secondElfBegin <= firstElfBegin && firstElfBegin <= secondElfEnd) ||
            (secondElfBegin <= firstElfBegin && firstElfBegin <= secondElfEnd);

        return firstElfRedundant || secondElfRedundant ? count + 1 : count;
    }, 0)

    console.log(`The total number of fully contained, overlapping assignment pairs is: ${fullyOverlappingAssignmentPairCount}`)
    console.log(`The total number of assignment pairs with any overlap is: ${overlappingAssignmentPairCount}`)
}