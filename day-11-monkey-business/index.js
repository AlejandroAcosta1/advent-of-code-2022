const readFile = require("../read-file");

readFile().then(monkeyBusiness);

const util = require('util')
function monkeyBusiness(data) {
  const monkeyStrings = data.split("\n\n").filter((x) => x);

  // These functions mutate the argument
  const monkeys = noWorries(monkeyStrings.map(parseMonkey));
  const worriedMonkeys = allWorries(monkeyStrings.map(parseMonkey))

  const monkeyBusinessScore = businessScore(monkeys);
  const worriedBusinessScore = businessScore(worriedMonkeys);

  console.log(`The total monkey business resulting from all this stuff-slinging simian shenanigans is ${monkeyBusinessScore}`)
  console.log(`After 10000 rounds, this monkey business level is ${worriedBusinessScore}`)
}

function businessScore(monkeys) {
  const inspectedCount = monkeys.map(monkey => monkey.inspected).sort((a, b) => b - a);
  return inspectedCount.slice(0, 2).reduce((sum, count) => sum * count);
}

function noWorries(monkeys) {
  const totalRounds = 20;
  for (let round = 1; round <= totalRounds; round++) {
    for (const monkey of monkeys) {
      while (monkey.items.length) {
        const item = monkey.items.shift();
        const inspectedValue = inspect(monkey, item)
        const newItem = Math.trunc(inspectedValue / 3);
        monkey.inspected += 1;

        const { divisibleBy, trueTarget, falseTarget } = monkey.test;
        const isDivisibleBy = newItem % divisibleBy === 0;
        const targetMonkey = isDivisibleBy ? trueTarget : falseTarget;

        monkeys[targetMonkey].items.push(newItem);
      }
    }
  }
  return monkeys;
}


function allWorries(monkeys) {
  const totalRounds = 10000;
  const lcm = monkeys.reduce((product, monkey) => product * monkey.test.divisibleBy, 1)
  for (let round = 1; round <= totalRounds; round++) {
    for (const monkey of monkeys) {
      while (monkey.items.length) {
        const item = monkey.items.shift();
        const newItem = inspect(monkey, item);

        const { divisibleBy, trueTarget, falseTarget } = monkey.test;

        monkey.inspected += 1;

        const isDivisibleBy = newItem % divisibleBy === 0;
        const targetMonkey = isDivisibleBy ? trueTarget : falseTarget;

        const modulatedWorry = modulateWorry(newItem, lcm);

        monkeys[targetMonkey].items.push(modulatedWorry);
      }
    }
  }
  return monkeys;
}

function modulateWorry(amount, modulo) {
  let modulated = amount;
  for (let k = 1; modulated > modulo; k++) {
    modulated = amount - modulo * k;
  }
  return modulated;
}

function inspect(monkey, item) {
  const { operation } = monkey;
  let { lhs, result, operator, rhs } = operation;
  if (result !== "new")
    console.error("Unsupported operation. Assigning to new");
  if (lhs === "old") lhs = item;
  if (rhs === "old") rhs = item;

  switch (operator) {
    case "+":
      return lhs + rhs;
    case "*":
      return lhs * rhs;
  }
}

function parseMonkey(monkey) {
  const [title, startingItems, operation, ...test] = monkey.split("\n");

  const monkeyIndex = parseInt(title.match(/Monkey (\d):/)[1]);

  return {
    index: parseInt(monkeyIndex),
    items: parseStartingItems(startingItems),
    operation: parseOperation(operation),
    test: parseTest(test),
    inspected: 0,
  };
}

function parseStartingItems(string) {
  const [, list] = string.split(":");
  return list.split(",").map((item) => parseInt(item));
}

function parseOperation(string) {
  const [, operation] = string.split(":");
  let [, result, lhs, operator, rhs] = operation.match(
    /(\w+) = (\w+) (.) (\w+)/
  );
  if (lhs !== "old" && lhs !== "new") lhs = parseInt(lhs);
  if (rhs !== "old" && rhs !== "new") rhs = parseInt(rhs);

  return {
    lhs,
    result,
    operator,
    rhs,
  };
}

function parseTest(lines) {
  const [condition, trueClause, falseClause] = lines.map(
    (line) => line.split(":")[1]
  );

  const [, divisibleBy] = condition.match(/divisible by (\d+)/);
  const [, trueTarget] = trueClause.match(/throw to monkey (\d+)/);
  const [, falseTarget] = falseClause.match(/throw to monkey (\d+)/);

  return {
    divisibleBy: parseInt(divisibleBy),
    trueTarget: parseInt(trueTarget),
    falseTarget: parseInt(falseTarget),
  };
}
