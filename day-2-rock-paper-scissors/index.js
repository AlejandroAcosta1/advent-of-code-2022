const readFile = require("../read-file");

readFile().then(rockPaperScissors);

const enemyRock = "A";
const enemyPaper = "B";
const enemyScissors = "C";
const playerRock = "X";
const playerPaper = "Y";
const playerScissors = "Z";
const wantLoss = "X";
const wantDraw = "Y";
const wantVictory = "Z";
const loss = 0;
const draw = 3;
const victory = 6;

const outcomes = {
    [enemyRock]: {
        [playerRock]: draw,
        [playerPaper]: victory,
        [playerScissors]: loss,
    },
    [enemyPaper]: {
        [playerRock]: loss,
        [playerPaper]: draw,
        [playerScissors]: victory,
    },
    [enemyScissors]: {
        [playerRock]: victory,
        [playerPaper]: loss,
        [playerScissors]: draw,
    },
};

const chosenMove = {
    [enemyRock]: {
        [wantLoss]: playerScissors,
        [wantDraw]: playerRock,
        [wantVictory]: playerPaper,
    },
    [enemyPaper]: {
        [wantLoss]: playerRock,
        [wantDraw]: playerPaper,
        [wantVictory]: playerScissors,
    },
    [enemyScissors]: {
        [wantLoss]: playerPaper,
        [wantDraw]: playerScissors,
        [wantVictory]: playerRock,
    },
};

const outcomePoints = {
    [wantLoss]: loss,
    [wantDraw]: draw,
    [wantVictory]: victory,
}

const usagePoints = {
    [playerRock]: 1,
    [playerPaper]: 2,
    [playerScissors]: 3,
};

function rockPaperScissors(data) {
    const moves = data
        .split("\n")
        .filter(x => x)
        .map(line => line.split(" "));

    const totalScore = moves.reduce((score, move) => {
        const [theirMove, myMove] = move;
        const outcome = outcomes[theirMove][myMove];
        const useScore = usagePoints[myMove];
        const roundScore = outcome + useScore;

        return score + roundScore;
    }, 0);

    const correctedScore = moves.reduce((score, move) => {
        const [theirMove, desiredOutcome] = move;
        const myMove = chosenMove[theirMove][desiredOutcome];

        const usageScore = usagePoints[myMove];
        const outcomeScore = outcomePoints[desiredOutcome];
        const roundScore = usageScore + outcomeScore;

        return score + roundScore;
    }, 0)

    console.log(`The strategy guide will get you a score of ${totalScore}.`)
    console.log(`The corrected strategy guide will get you a score of ${correctedScore}`)

}
