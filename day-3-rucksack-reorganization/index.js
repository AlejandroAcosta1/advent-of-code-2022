const readFile = require("../read-file");

readFile().then(rucksackReorganization);

function rucksackReorganization(data) {
    const rucksacks = data
        .split("\n")
        .filter(x => x);

    const misplaced = findMisplaced(rucksacks);
    const totalPriority = misplaced.reduce((sum, priority) => sum + priority);

    const badges = findBadges(rucksacks);
    const badgePriority = badges.reduce((sum, priority) => sum + priority);

    console.log(`The sum of the priorities for the misplaced items is: ${totalPriority}`)
    console.log(`The sum of the priorities for the badges are: ${badgePriority}`)
}

function findMisplaced(rucksacks) {
    return rucksacks.map(rucksack => {
        const [compartment1, compartment2] = splitSack(rucksack);

        const seen = Array.from(compartment1).reduce((seenSoFar, letter) => {
            return {
                ...seenSoFar,
                [letter]: true,
            }
        }, {});

        const misplaced = Array.from(compartment2).find(letter => seen[letter])
        return getLetterValue(misplaced)
    });
}

function findBadges(rucksacks) {
    let groups = [];
    for (let i = 0; i < rucksacks.length; i += 3) {
        groups.push(rucksacks.slice(i, i + 3));
    }

    const badges = groups.map(group => {
        const seenCount = Array.from(group).reduce((countAcc, rucksack, index) => {
            for (const item of rucksack) {
                if (countAcc[item] === undefined) {
                    countAcc[item] = 0;
                }
                if (countAcc[item] === index) {
                    countAcc[item] += 1;
                }
            }
            return countAcc;
        }, {});

        const badge = Object.keys(seenCount).find(item => seenCount[item] === 3);
        return getLetterValue(badge);
    })
    return badges;
}

function splitSack(rucksack) {
    return [
        rucksack.substring(0, rucksack.length / 2),
        rucksack.substring(rucksack.length / 2)
    ];
}

function getLetterValue(letter) {
    if (letter >= "a") {
        return letter.charCodeAt(0) - "a".charCodeAt(0) + 1;
    } else {
        return letter.charCodeAt(0) - "A".charCodeAt(0) + 1 + 26;
    }
}