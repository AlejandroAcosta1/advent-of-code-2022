# Advent of Code 2022

Work for [Advent of Code challenge](https://adventofcode.com/2022)

## Running
Make sure the corresponding input.txt is in each puzzle's folder.

From the puzzle's directory run `node index.js input.txt`