const readFile = require("../read-file");

readFile().then(tuningTrouble);

function tuningTrouble(dataStream) {
    const startOfPacketWindowSize = 4;
    const startOfMessageWindowSize = 14;
    const processedCharacters = findMessageStarter(
            dataStream,
            startOfPacketWindowSize
            );
    const messageCharacters = findMessageStarter(
            dataStream,
            startOfMessageWindowSize
            );

    console.log(
            `${processedCharacters} characters need to be processed before the first start-of-packet marker is detected.`
            );
    console.log(
            `${messageCharacters} characters need to be processed before the first start-of-message marker is detected.`
            );
}

function findMessageStarter(dataStream, windowSize) {
    const window = [];
    let processedCharacters;
    for (let i = 0; i < dataStream.length; i++) {
        const potentialMarker = dataStream[i];
        if (window.length < windowSize) {
            window.push(potentialMarker);
            continue;
        }

        window.shift();
        window.push(potentialMarker);

        let hasDupes = false;
        const seen = {};
        for (let char of window) {
            if (seen[char]) {
                hasDupes = true;
            } else {
                seen[char] = true;
            }
        }

        if (!hasDupes) {
            processedCharacters = i + 1;
            break;
        }
    }
    return processedCharacters;
}