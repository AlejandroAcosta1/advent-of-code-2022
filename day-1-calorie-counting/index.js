const readFile = require("../read-file");

readFile().then(calorieCounting);

function calorieCounting(data) {
    const elves = data
        .split("\n\n")
        .map(elf =>
            elf.split("\n")
                .map(calories => calories ? parseInt(calories) : 0)
                .reduce((sum, calories) => sum + calories)
        );

    elves.sort((a, b) => b - a);
    const topThreeElves = elves.slice(0, 3).reduce((sum, elf) => sum + elf);

    console.log(`The elf carrying the most Calories is carrying ${elves[0]} Calories.`)
    console.log(`The top 3 elves are carrying ${topThreeElves} Calories.`)
}