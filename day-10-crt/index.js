const readFile = require("../read-file");

readFile().then(cathodeRays);

const commandCosts = {
  noop: 1,
  addx: 2,
};

const candidateCycles = [20, 60, 100, 140, 180, 220];

const screenWidth = 40;
const screenHeight = 6;

function cathodeRays(data) {
  const lines = data.split("\n").filter((x) => x);
  const commands = parseCommands(lines);

  const signalSum = candidateCycles.reduce((sum, cycleCount) => {
    const strength = strengthAt(commands, cycleCount);
    return sum + strength * cycleCount;
  }, 0);
  const screen = drawScreen(commands);

  console.log(`The sum of the six signal strengths is ${signalSum}`);
  console.log(screen);
}

function parseCommands(lines) {
  return lines.map((line) => {
    const [command, argument] = line.split(" ");
    return {
      command,
      argument: argument && parseInt(argument),
    };
  });
}

function drawScreen(commands) {
  let screen = [];
  const pixelCount = screenWidth * screenHeight;
  for (let clock = 1; clock <= pixelCount; clock++) {
    const strength = strengthAt(commands, clock);
    const position = strength;
    const drawnPosition = (clock - 1) % screenWidth;
    if (Math.abs(position - drawnPosition) <= 1) {
      screen.push("#");
    } else {
      screen.push(".");
    }
  }

  return finishScreen(screen);
}

function finishScreen(screen) {
  let display = [];
  for (let i = 0; i < screenHeight; i++) {
    display.push(screen.slice(screenWidth * i, screenWidth * (i + 1)).join(""));
  }

  return display.join("\n");
}

// Get signal strength after `cycles` cycles.
// If not provided, runs whole program
function strengthAt(commands, cycles) {
  let elapsedCycles = 0;
  let signalStrength = 1;
  for (let i = 0; i < commands.length; i++) {
    const { command, argument } = commands[i];
    elapsedCycles += commandCosts[command];

    if (cycles !== undefined && elapsedCycles >= cycles) {
      break;
    }

    if (argument) {
      signalStrength += argument;
    }
  }

  return signalStrength;
}
