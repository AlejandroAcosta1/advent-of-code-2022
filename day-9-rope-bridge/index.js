const readFile = require("../read-file");

readFile().then(ropeBridge);

const ropeSize = 10;
function ropeBridge(data) {
  const movements = data.split("\n").filter((x) => x);
  let rope = new Array(ropeSize).fill({ x: 0, y: 0 });
  let headPosition = { x: 0, y: 0 };
  let tailPosition = { x: 0, y: 0 };

  let seenFirstKnotPositions = {};
  let seenTailPositions = {};
  movements.forEach((movement) => {
    const [direction, amountString] = movement.split(" ");
    const amount = parseInt(amountString);

    for (let i = 0; i < amount; i++) {
      rope[0] = moveHead(rope[0], direction);

      for (let j = 1; j < ropeSize; j++) {
        const shouldDragKnot = shouldDrag(rope[j - 1], rope[j]);
        if (shouldDragKnot) {
          rope[j] = moveTail(rope[j - 1], rope[j]);
        }

        // Track part 1 and the 1 knot solution
        if (j === 1) {
          const key = coordinateToString(rope[1]);
          seenFirstKnotPositions[key] = true;
        }
      }

      const seenKey = coordinateToString(rope[ropeSize - 1]);
      seenTailPositions[seenKey] = true;
    }
  });

  const firstKnotPositions = countPositions(seenFirstKnotPositions);
  const lastKnotPositions = countPositions(seenTailPositions);

  console.log(
    `The number of unique positions the tail of the rope visits is ${firstKnotPositions}`
  );
  console.log(
    `The number of unique positions that the long tail of the rope visits is ${lastKnotPositions}`
  );
}

function countPositions(knotPositions) {
  return Object.keys(knotPositions).reduce((count, key) => {
    return knotPositions[key] ? count + 1 : count;
  }, 0);
}

function ropeBridgeOld(data) {
  const movements = data.split("\n").filter((x) => x);
  let headPosition = { x: 0, y: 0 };
  let tailPosition = { x: 0, y: 0 };

  let seenPositions = {};
  movements.forEach((movement) => {
    const [direction, amountString] = movement.split(" ");
    const amount = parseInt(amountString);

    for (let i = 0; i < amount; i++) {
      headPosition = moveHead(headPosition, direction);
      const dragsTail = shouldDrag(headPosition, tailPosition);
      if (dragsTail) {
        tailPosition = moveTail(headPosition, tailPosition);
      }
      const seenKey = coordinateToString(tailPosition);
      seenPositions[seenKey] = true;
    }
  });

  const tailPositions = Object.keys(seenPositions).reduce((count, key) => {
    return seenPositions[key] ? count + 1 : count;
  }, 0);

  console.log(
    `The number of unique positions the tail of the rope visits is ${tailPositions}`
  );
}

function moveHead(location, direction) {
  switch (direction) {
    case "U":
      return { x: location.x, y: location.y + 1 };
    case "D":
      return { x: location.x, y: location.y - 1 };
    case "R":
      return { x: location.x + 1, y: location.y };
    case "L":
      return { x: location.x - 1, y: location.y };
  }
}

function coordinateToString(position) {
  return [position.x, position.y].join(",");
}

function shouldDrag(head, tail) {
  return Math.abs(head.x - tail.x) > 1 || Math.abs(head.y - tail.y) > 1;
}

function moveTail(head, tail) {
  // Dragged up
  if (head.x === tail.x && head.y > tail.y) {
    return { ...tail, y: tail.y + 1 };
  }
  // Dragged down
  else if (head.x === tail.x && head.y < tail.y) {
    return { ...tail, y: tail.y - 1 };
  }
  // dragged right
  else if (head.y === tail.y && head.x > tail.x) {
    return { ...tail, x: tail.x + 1 };
  }
  // dragged left
  else if (head.y === tail.y && head.x < tail.x) {
    return { ...tail, x: tail.x - 1 };
  }
  // northeast
  else if (head.x > tail.x && head.y > tail.y) {
    return { x: tail.x + 1, y: tail.y + 1 };
  }
  // southwest
  else if (head.x < tail.x && head.y < tail.y) {
    return { x: tail.x - 1, y: tail.y - 1 };
  }
  // northwest
  else if (head.x < tail.x && head.y > tail.y) {
    return { x: tail.x - 1, y: tail.y + 1 };
  }
  // southeast
  else if (head.x > tail.x && head.y < tail.y) {
    return { x: tail.x + 1, y: tail.y - 1 };
  }
}
