const readFile = require("../read-file");

readFile().then(deviceSpace);

const filesystemSize = 70000000;
const neededFreeSpace = 30000000;

function deviceSpace(data) {
    const lines = data.split("\n").filter((x) => x);
    const commands = parseCommands(lines);
    const fileSystem = readFileSystem(commands);

    const dirSizes = getDirSizes("/", fileSystem["/"])
    const usedSpace = dirSizes.slice(-1)[0].size;
    const unusedSpace = filesystemSize - usedSpace;
    const neededSpace = neededFreeSpace - unusedSpace;

    const largeEnoughDirs = dirSizes.filter(({size}) => size >= neededSpace)
    const smallestDirSize = largeEnoughDirs.reduce((smallest, {size}) => Math.min(smallest, size), filesystemSize)

    const smallDirs = listSmallDirs(dirSizes);
    const totalSize = smallDirs.reduce((acc, {size}) => acc + size, 0);

    console.log(`The sum of directories with a filesize of less than 100,000 is ${totalSize}`);
    console.log(`The total size of the drive to delete to free up enough space is ${smallestDirSize}`)
}

function listSmallDirs(directories) {
    const largeDir = 100000;
    const suspectDirs = directories.filter(({size}) => size <= largeDir);

    return suspectDirs;
}

function getDirSizes(name, contents, directories = []) {
    let size = 0;
    for (const file in contents) {
        const filesizeOrDirContents = contents[file];
        if (typeof filesizeOrDirContents === "number") {
            size += filesizeOrDirContents;
        } else {
            const subdirs = getDirSizes(file, filesizeOrDirContents, directories);
            size += subdirs.slice(-1)[0].size;
        }
    }

    directories.push({name, size})
    return directories;
}

function getDirSizes2(tree, directories = []) {
    console.log({tree, directories});
    let size = 0;
    for (const key in tree) {
        const value = tree[key];

        if (typeof value === "number") {
            size += value;
        } else {
            size += getDirSizes(value, directories);
            directories.push({name: key, size});
        }
        console.log({value, key});
    }

    return directories;
}

function parseCommands(lines) {
    return lines.reduce((acc, line) => {
        if (line.indexOf("$") === 0) {
            acc.push({command: line.replace(/^\$ /, ""), output: []});
        } else {
            acc.slice(-1)[0].output.push(line);
        }
        return acc;
    }, []);
}

function readFileSystem(commands) {
    let pwd = "";
    return commands.reduce((tree, commandDetails) => {
        const {command, output} = commandDetails;
        if (command === "ls") {
            output.forEach((item) => {
                const [dirOrSize, name] = item.split(" ");
                if (dirOrSize === "dir") {
                    // do nothing
                } else {
                    const filesize = dirOrSize;
                    set(tree, pwd, {key: name, value: parseInt(filesize)});
                }
            });
        } else {
            const [commandType, dirName] = command.split(" ");
            if (commandType === "cd") {
                pwd = changeDirectory(pwd, dirName);
            }
        }
        return tree;
    }, {});
}

function changeDirectory(pwd, dirName) {
    if (dirName === "/") {
        return "/";
    } else if (dirName === "..") {
        return pwd.split("/").slice(0, -1).join("/");
    } else {
        return [pwd, dirName].join("/").replace("//", "/");
    }
}

function pathParse(path) {
    return path === "/" ? ["/"] : path.split("/").map((x) => (x ? x : "/"));
}

// Autovivifying get
function get(tree, path) {
    let subtree = tree;

    const segments = pathParse(path);

    for (const segment of segments) {
        if (!subtree[segment]) {
            subtree[segment] = {};
        }
        subtree = subtree[segment];
    }

    return subtree;
}

// Autovivifying set
function set(tree, path, {key, value}) {
    let subtree = tree;

    const segments = pathParse(path);

    for (const segment of segments) {
        if (!subtree[segment]) {
            subtree[segment] = {};
        }
        subtree = subtree[segment];
    }

    subtree[key] = value;
}

